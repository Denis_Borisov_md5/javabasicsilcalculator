package models;

public class Input {
    /*
     * для тестов и дальнейшей разработки
     */
    private String inputString;

    public Input(String inputString) {
        this.inputString = inputString;
    }

    public String getInputString() {
        return inputString;
    }

    public void setInputString(String inputString) {
        this.inputString = inputString;
    }
}
