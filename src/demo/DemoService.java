package demo;

import service.CalculateService;
import service.GetOPNService;
import service.ICalculateService;
import service.IGetOPNService;

public class DemoService implements IDemoService{
    @Override
    public void execute(){
        ICalculateService calculateService = new CalculateService();
        IGetOPNService getOPNService = new GetOPNService();

        String inputString = "5 + ( 1 + 2 ) *  4 - 5";

        //String inputString = "( 1 + 2 ) * ( 4 + 3 ) / 4 - 58 + 99 / 3";
        System.out.println("Input string: " + inputString);

        System.out.println("Result: " + calculateService.calculateResult(getOPNService.getTokenString(inputString)));
    }
}
