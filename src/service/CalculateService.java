package service;

import java.util.Stack;

public class CalculateService implements ICalculateService{
    private  ICheckService checkService = new CheckService();

    @Override
    public double calculateResult(String tokenString){
        Stack<String> stackOfStringTokens = new Stack<String>();

        for (String currentSymbol : tokenString.split(" ")) {
            if (checkService.isNumber(currentSymbol)) {
                stackOfStringTokens.push(currentSymbol);
                continue;
            }

            if (checkService.isOperator(currentSymbol)) {
                double result = 0;
                double first = Double.parseDouble(stackOfStringTokens.pop());
                double second = Double.parseDouble(stackOfStringTokens.pop());

                switch (currentSymbol) {
                    case "/":
                        result = second / first;
                        break;
                    case "*":
                        result = second * first;
                        break;
                    case "+":
                        result = second + first;
                        break;
                    case "-":
                        result = second - first;
                        break;
                }
                stackOfStringTokens.push(String.valueOf(result));
            }
        }
        return Double.parseDouble(stackOfStringTokens.pop());
    }
}
