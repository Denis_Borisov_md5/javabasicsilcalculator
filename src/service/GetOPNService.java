package service;

import java.util.LinkedList;
import java.util.Stack;

public class GetOPNService implements IGetOPNService {
    private  ICheckService checkService = new CheckService();

    @Override
    public String getTokenString(String inputString){
        LinkedList<String> tokenString = new LinkedList<>();
        Stack<String> tokenStack = new Stack<>();

        //проходим по строке
        for(String currentSymbol : inputString.split(" ")){
            //если находим число - сразу записывем в строку токенов
            if (checkService.isNumber(currentSymbol)){
                tokenString.add(currentSymbol + " ");
                continue;
            }

            //скобка в стек
            if (currentSymbol.equals("(") || tokenStack.empty()){
                tokenStack.push(currentSymbol);
                continue;
            }

            /*
             * если закрывающая скобка, то извлекаем символы из
             * стека в строку токенов до тех пор,
             * пока не встретим открывающую скобку.
             */
            if (currentSymbol.equals(")")) {
                while (!tokenStack.peek().equals("(")) {
                    tokenString.add(tokenStack.pop() + " ");
                }
                tokenStack.pop();//выталкиваем саму скобку.
                continue;
            }

            /*
             * если верхний в стеке оператор имеет больший
             * приоритет, чем приоритет текущего оператора, то
             * извлекаем символы из стека в строку токенов
             * до тех пор, пока выполняется условие
             */
            if (checkService.isOperator(currentSymbol)) {
                while (!tokenStack.isEmpty()
                        && checkService.priorytyCheck(tokenStack.peek()) >= checkService.priorytyCheck(currentSymbol)) {
                    tokenString.add(tokenStack.pop() + " ");
                }
                tokenStack.push(currentSymbol);
            }
        }

        /*
         * отложенные в стеке операторы добавляем
         * в строку токенов.
         */
        while (!tokenStack.empty()) {
            tokenString.add(tokenStack.pop() + " ");
        }

        // сбор готовой строки
        StringBuilder opnString = new StringBuilder();
        for (String s : tokenString)
            opnString.append(s);

        return opnString.toString();
    }
}
