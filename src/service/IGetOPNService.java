package service;

import models.Input;

public interface IGetOPNService {
    String getTokenString(String inputString);
}
