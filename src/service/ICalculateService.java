package service;

public interface ICalculateService {
    double calculateResult(String tokenString);
}
