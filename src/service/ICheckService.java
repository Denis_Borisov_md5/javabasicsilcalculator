package service;

public interface ICheckService {
    boolean isNumber(String currentSymbol);
    boolean isOperator(String currentSymbol);
    int priorytyCheck(String currentSymbol);
}
